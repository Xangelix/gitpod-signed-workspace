FROM registry.gitlab.com/xangelix/gitpod-signed-workspace:defaultvnc-latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

RUN sudo apt update && sudo apt install -yq texlive-full anki
