FROM registry.gitlab.com/xangelix/gitpod-signed-workspace:default-latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

RUN go install github.com/jesseduffield/lazydocker@latest

ENV PY_VER=pypy3.9

RUN pyenv update ; pyenv install $PY_VER:latest && \
    pyenv global \
    $( \
    pyenv global system && \
    pyenv versions | grep $PY_VER | sort -r | sed -n 1p | cut -d " " -f 3 \
    ) || true

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir poetry

RUN poetry config virtualenvs.in-project true
