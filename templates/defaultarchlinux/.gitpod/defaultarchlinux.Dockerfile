FROM docker.io/archlinux:base-devel
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

RUN pacman -Syu --noconfirm git git-lfs docker

#### Taken From gitpod/workspace-base with slight modifications
### Gitpod user ###
RUN useradd --no-log-init --uid 33333 --user-group --groups wheel --create-home --home-dir /home/gitpod --shell /bin/bash --password gitpod gitpod
RUN sed -i 's!# %wheel ALL=(ALL:ALL) NOPASSWD: ALL!%wheel ALL=(ALL:ALL) NOPASSWD: ALL!g' /etc/sudoers

ENV HOME=/home/gitpod
WORKDIR $HOME

### Gitpod user (2) ###
USER gitpod
# use sudo so that user does not get sudo usage info on (the first) login
RUN sudo echo "Running 'sudo' for Gitpod: success"
RUN mkdir /home/gitpod/.bashrc.d
RUN (echo; echo "for i in \$(ls \$HOME/.bashrc.d/*); do source \$i; done"; echo) >> /home/gitpod/.bashrc

# configure git-lfs
RUN sudo git lfs install --system
####

# add yay for aur
RUN sudo su gitpod -c cd /tmp && \
    git clone https://aur.archlinux.org/yay-bin && \
    cd yay-bin && \
    makepkg -si --noconfirm
RUN sudo su gitpod -c "yay -Syu --noconfirm fastfetch"
