FROM docker.io/gitpod/workspace-base:latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

ENV DEBIAN_FRONTEND=noninteractive

RUN echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections && \
    echo keyboard-configuration keyboard-configuration/layout select 'English (US)' | sudo debconf-set-selections && \
    echo keyboard-configuration keyboard-configuration/layoutcode select 'us' | sudo debconf-set-selections && \
    echo 'resolvconf resolvconf/linkify-resolvconf boolean false' | sudo debconf-set-selections

RUN mkdir ~/.fonts && \
    curl -L "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.1/FiraCode.zip" -o FiraCode.zip && \
    unzip FiraCode.zip -d ~/.fonts/ && \
    rm FiraCode.zip && \
    fc-cache -fv

RUN sudo apt update && sudo apt upgrade -yq && sudo apt dist-upgrade -y && \
    sudo apt -yq install debconf-utils

USER root

# Install Desktop-ENV, tools and google-chrome
RUN cd /tmp && glink="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb" \
    && wget -q "$glink" \
    && install-packages \
    tigervnc-standalone-server tigervnc-xorg-extension \
    dbus dbus-x11 gnome-keyring xfce4 xfce4-terminal \
    xdg-utils x11-xserver-utils pip ./"${glink##*/}" \
    && ln -srf /usr/bin/google-chrome /usr/bin/chromium \
    # Extra chrome tweaks
    ## Disables welcome screen
    && t="$HOME/.config/google-chrome/First Run" && sudo -u gitpod mkdir -p "${t%/*}" && sudo -u gitpod touch "$t" \
    ## Disables default browser prompt
    && t="/etc/opt/chrome/policies/managed/managed_policies.json" && mkdir -p "${t%/*}" && printf '{ "%s": %s }\n' DefaultBrowserSettingEnabled false > "$t"
# OLD: && ln -srf /usr/bin/chromium /usr/bin/google-chrome
# OLD: To make ungoogled_chromium discoverable by tools like flutter

# Install novnc and numpy module for it
RUN git clone --depth 1 https://github.com/novnc/noVNC.git /opt/novnc \
    && git clone --depth 1 https://github.com/novnc/websockify /opt/novnc/utils/websockify \
    && find /opt/novnc -type d -name '.git' -exec rm -rf '{}' + \
    && sudo -H pip3 install numpy
ADD https://raw.githubusercontent.com/gitpod-io/workspace-images/main/chunks/tool-vnc/novnc-index.html /opt/novnc/index.html

# Add VNC startup script
ADD https://raw.githubusercontent.com/gitpod-io/workspace-images/main/chunks/tool-vnc/gp-vncsession /usr/bin/gp-vncsession
RUN chmod 0755 "$(which gp-vncsession)" \
    && printf '%s\n' 'export DISPLAY=:0' \
    'gp-vncsession' >> "$HOME/.bashrc"

# Add X11 dotfiles
ADD --chown=gitpod:gitpod https://raw.githubusercontent.com/gitpod-io/workspace-images/main/chunks/tool-vnc/.xinitrc $HOME/

USER gitpod
