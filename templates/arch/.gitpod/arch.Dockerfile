FROM registry.gitlab.com/xangelix/gitpod-signed-workspace:default-latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

RUN go install github.com/jesseduffield/lazydocker@latest
