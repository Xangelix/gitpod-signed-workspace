FROM registry.gitlab.com/xangelix/gitpod-signed-workspace:default-latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

RUN echo 'alias vg="valgrind --tool=memcheck --leak-check=yes -q --track-origins=yes"' >> ~/.bashrc

RUN sudo apt install -yq clang clang-format llvm valgrind
