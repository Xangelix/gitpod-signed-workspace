FROM registry.gitlab.com/xangelix/gitpod-signed-workspace:defaultvnc-latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"

SHELL ["/bin/bash", "-c"]

ENV ANDROID_HOME=/home/gitpod/androidsdk
ENV CMDLINE_TOOLS_VER="8512546"
ENV PATH="${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools:${ANDROID_HOME}/emulator:/opt/flutter/bin:${PATH}"

RUN sudo apt update && sudo apt install -y p7zip-full libasound2-dev libgtk-3-dev libnss3-dev fonts-noto fonts-noto-cjk android-sdk && \
    sudo git clone --branch beta https://github.com/flutter/flutter.git /opt/flutter && sudo chown -R gitpod /opt/flutter && \
    mkdir -p ${ANDROID_HOME}/cmdline-tools && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-${CMDLINE_TOOLS_VER}_latest.zip && \
    unzip *tools*linux*.zip -d ${ANDROID_HOME}/cmdline-tools && \
    rm *tools*linux*.zip && \
    mv ${ANDROID_HOME}/cmdline-tools/cmdline-tools ${ANDROID_HOME}/cmdline-tools/latest && \
    yes | sdkmanager "platform-tools" "platforms;android-33" "emulator" && \
    yes | sdkmanager "system-images;android-33;google_apis;x86_64" && \
    yes | sdkmanager "build-tools;30.0.2" && \
    yes | flutter doctor --android-licenses && \
    echo no | avdmanager create avd -n avd28 -k "system-images;android-33;google_apis;x86_64" && \
    flutter config --android-sdk $ANDROID_HOME && \
    flutter --version && \
    flutter config --enable-linux-desktop && \
    flutter config --enable-macos-desktop
