#!/usr/bin/env bash
shopt -s dotglob

template_keys=("default" "defaultvnc" "notes" "python" "flutter" "c")

if [ "$1" == "init" ]; then
    git clone https://gitlab.com/Xangelix/gitpod-signed-workspace
    if [ "$2" == "notes" ]; then
        cp -r ./gitpod-signed-workspace/templates/notes/* .
    elif [ "$2" == "python" ]; then
        cp -r ./gitpod-signed-workspace/templates/python/* .
    elif [ "$2" == "flutter" ]; then
        cp -r ./gitpod-signed-workspace/templates/flutter/* .
    elif [ "$2" == "c" ]; then
        cp -r ./gitpod-signed-workspace/templates/c/* .
    elif [ "$2" == "defaultvnc" ]; then
        cp -r ./gitpod-signed-workspace/templates/defaultvnc/* .
    elif [ "$2" == "arch" ]; then
        cp -r ./gitpod-signed-workspace/templates/arch/* .
    elif [ "$2" == "docker" ]; then
        cp -r ./gitpod-signed-workspace/templates/docker/* .
    elif [ "$2" == "defaultarchlinux" ]; then
        cp -r ./gitpod-signed-workspace/templates/defaultarchlinux/* .
    else
        if [ "$2" != "default" ]; then
            echo "No template specified, loading default."
        fi
        cp -r ./gitpod-signed-workspace/templates/default/* .
    fi
    rm -rf ./gitpod-signed-workspace/
    sudo chmod +x ./.gitpod.update.sh

elif [ "$1" == "list" ]; then
    echo "==Templates=="
    for template_key in ${template_keys[@]}; do
        echo $template_key
    done
else
    echo "No valid arguments read."
fi
