# gitpod-signed-workspace

This repo serves as a model defining the provisioning an Ubuntu 20.04.4 LTS Gitpod container and Theia client with configured secrets:

- SSH Private Key
- GPG Key

## WARNING--ANYONE YOU SHARE YOUR GITPOD INSTANCE WITH WILL HAVE ACCESS TO YOUR UNENCRYPTED GPG AND SSH KEYS

## Usage

### GPG Config

List GPG2 private keys.

```bash
gpg2 --list-secret-keys --keyid-format LONG user@email.com
```

Example output:

```
sec   rsa4096/12345ABCD12345AB 2021-01-22 [SC]
      HGSG9089FKJSGSG980SF098DSDGSG9890908SDFG
uid                 [ultimate] First Middle Last <user@email.com>
ssb   rsa4096/ASFAEG9809087ADF 2099-01-01 [E]

```

In the above example, the key ID is 12345ABCD12345AB.

Copy and paste this into a Gitpod environment variable `GPG_KEY_ID`.

Export the key based on this ID.

```bash
gpg2 --export-secret-keys YOUR_ID_HERE | base64
```

Copy and paste this into a Gitpod environment variable `GPG_KEY_B64`.

Finally, create another Gitpod environment variable `GIT_EMAIL` and set it to your key's email.

### SSH Config

Encode your private key to base64, usually located at `~/.ssh/id_rsa`

```bash
base64 < ~/.ssh/id_rsa
```

Copy and paste this into a Gitpod environment variable `SSH_KEY_B64`.

### Gitpod Config

For each of these environment variables it is recommended that you limit the scope by least privilege.

### Filesystem

Execute the command below to install this filesystem unto your project. This command can also be executed to update your installation.

```bash
curl -fsSL https://gitlab.com/Xangelix/gitpod-signed-workspace/-/raw/master/.gitpod.update.sh -o ./.gitpod.update.sh; sudo chmod +x ./.gitpod.update.sh; ./.gitpod.update.sh init
```

Alternative Init

```bash
curl -fsSL https://gitlab.com/Xangelix/gitpod-signed-workspace/-/raw/master/.gitpod.init.sh -o ./.gitpod.init.sh; sudo chmod +x ./.gitpod.init.sh; ./.gitpod.init.sh
```

Init Helper

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/Xangelix/gitpod-init-helper)

---

## Dev Environment

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/Xangelix/gitpod-signed-workspace)

### Markdown Editor

- [Hotkeys](https://ld246.com/article/1582778815353)
